# README #

### What is this repository for? ###

This is the AlergAgility main website repo.

### How do I get set up? ###

* Clone this repo
* Make sure you have gohugo installed in your GO environment.

### Testing your changes.
To test your changes in, run the following command in your repo.

hugo server --port=8080 -b "sandeep-docs.alertagility.com" --appendPort=false --watch=true

^ command will start hugo dev web server where you can test the website.

### Contribution guidelines ###

To update the website clone this repo and push an update into 'master' branch.