+++
draft = false
title = "Terms of Service"

+++

##	ALERTAGILITY TERMS OF USE

Welcome to the AlertAgility website ("Website"). By accessing, browsing or using our websites,
you are agreeing to comply with and be bound by the following Terms of Use
(collectively the “Terms”) and all terms and conditions incorporated by reference.

Please review these Terms carefully before using the website and information provided
by AlertAgility Ltd., its subsidiaries and affiliates (hereinafter, “AlertAgility”).

IF YOU DO NOT AGREE TO THESE TERMS, YOU SHOULD NOT USE OUR WEBSITE(S).

The Website is owned and operated by AlertAgility Ltd. The Website is offered subject to your
acceptance without modification of all of the terms and conditionals contained herein and
all other operating rules, policies (including, without limitation, AlertAgility's Privacy Policy)
and procedures that may be published from time to time on our websites by AlertAgility Ltd.

The Website is offered and available to users who are 18 years of age or older.
By using the The Website, you represent and warrant that you are of legal age to form a
binding contract with us. If you are using The Website(s) on behalf of a corporation or
other business entity (“Customer”), you represent that you are an authorized representative
of such Customer who is able to enter into a binding contract on behalf of the Customer.
In such a case the terms “you” and “your” refers to the Customer. If you do not meet these
requirements, you must not access or use AlertAgility Website and it's provided services.

AlertAgility may without notice to you, at any time amend these Terms and any other
information contained on this website.  The latest Terms will be posted on our websites,
and you should review the Terms prior to using the website. Your continued use of the websites
after any changes to these Terms are posted will be considered acceptance of those changes. 

The materials contained in this web site are protected by applicable copyright and trade mark law.

## 2. Use License
Permission is granted to temporarily download one copy of the materials
(information or software) on AlertAgility's web site for personal, 
non-commercial transitory viewing only. This is the grant of a license, 
not a transfer of title, and under this license you may not:		

1. modify or copy the materials;</li>
* use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
* attempt to decompile or reverse engineer any software contained on AlertAgility's web site;
* remove any copyright or other proprietary notations from the materials; or
* transfer the materials to another person or "mirror" the materials on any other server.

This license shall automatically terminate if you violate any of these restrictions and may be terminated by AlertAgility at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.


## 3. Disclaimer
DISCLAIMER OF WARRANTIES

Your use of and access to the AlertAgility's Website(s), Online Mediums and content posted by
AlertAgility, it's subsidiaries or user generated content posted by third parties is at your sole
risk. This Website and other websites under alertagility.com domain, and Content are provided for
informational purposes only on an "AS IS" and "AS AVAILABLE" basis without any express or implied
warranty of any kind, including warranties of merchantability, non-infringement, or fitness for
any particular purpose. AlertAgility makes no representations, warranties or gurantees as to the
quality, suitability, truth, accuracy or completeness of the Content. AlertAgility hereby further
makes no disclaims and negates all other warranties, including without limitation, implied warranties
or conditions of merchantability, fitness for a particular purpose, or non-infringement of
intellectual property or other violation of rights.

## 4. Limitations
In no event shall AlertAgility or its suppliers be liable for any damages (including, without
limitation, damages for loss of data or profit, or due to business interruption,) arising out of
the use or inability to use the materials on AlertAgility's Internet site, even if AlertAgility
or a AlertAgility authorized representative has been notified orally or in writing of the
possibility of such damage. Because some jurisdictions do not allow limitations on implied
warranties, or limitations of liability for consequential or incidental damages, these limitations
may not apply to you.
			
## 5. Revisions and Errata
The materials appearing on AlertAgility's web site could include technical, typographical, or
photographic errors. AlertAgility does not warrant that any of the materials on its web site
are accurate, complete, or current. AlertAgility may make changes to the materials contained on
its web site at any time without notice. AlertAgility does not, however, make any commitment
to update the materials.

## 6. Links
AlertAgility has not reviewed all of the sites linked to its Internet web site and is not responsible
for the contents of any such linked site. The inclusion of any link does not imply endorsement
by AlertAgility of the site. Use of any such linked web site is at the user's own risk.

## 7. Site Terms of Use Modifications
AlertAgility may revise these terms of use for its web site at any time without notice.
By using this web site you are agreeing to be bound by the then current version of these
Terms and Conditions of Use.

## 8. Governing Law
Any claim relating to AlertAgility's web site shall be governed by the laws of the State of
London without regard to its conflict of law provisions.

General Terms and Conditions applicable to Use of a Web Site.
## Privacy Policy
Your privacy is very important to us. Accordingly, we have developed this Policy in order for
you to understand how we collect, use, communicate and disclose and make use of personal information.
The following outlines our privacy policy.

* Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.	
* We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.
* We will only retain personal information as long as necessary for the fulfillment of those purposes. 
* We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned. 
* Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date. 
* We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.
* We will make readily available to customers information about our policies and practices relating to the management of personal information. 

We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. 
